Feature: Loading items from a data source (file, database, web API, ...)
  In order to be able to store items permanently
  As a developer
  I want to load them from a data source

  Scenario: Loading an item from a YAML file
    Given I have a YAML file containing multiple items
    When I tell the item manager to load the file
    Then it should contain "371" items
    And it should contain the following items: "Stone, Grass Block, Sand, Gold Ore, Note Block, Piston"
