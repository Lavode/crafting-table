Feature: Searching recipes by various criteria
  In order to easily look up recipes
  As a developer
  I want to be able to search for recipes based on various criteria.

  Background:
    Given I have the vanilla items loaded
    And I have the vanilla recipes loaded

  Scenario Outline: Searching for recipe's name
    Given I use exact matching: <exact_match>
    And use case sensitivity: <case_sensitive>
    When I search for the recipe's name: <name>
    Then the result should contain the following recipe: <recipe_name>

  Examples: Case-sensitive, exact matching
    | name  | exact_match | case_sensitive | recipe_name |
    | Stone | yes         | yes            | Stone       |
    | Torch | yes         | yes            | Torch       |
    | torch | yes         | yes            |             |
    | Tor   | yes         | yes            |             |

  Examples: Case-insensitive, exact matching
    | name  | exact_match | case_sensitive | recipe_name |
    | Stone | yes         | no             | Stone       |
    | Torch | yes         | no             | Torch       |
    | torch | yes         | no             | Torch       |
    | Tor   | yes         | no             |             |

  Examples: Case-sensitive, non-exact matching
    | name  | exact_match | case_sensitive | recipe_name |
    | Stone | no          | yes            | Stone       |
    | Torch | no          | yes            | Torch       |
    | torch | no          | yes            |             |
    | Tor   | no          | yes            | Torch       |

  Examples: Case-insensitive, non-exact matching
    | name  | exact_match | case_sensitive | recipe_name |
    | Stone | no          | no             | Stone       |
    | Torch | no          | no             | Torch       |
    | torch | no          | no             | Torch       |
    | Tor   | no          | no             | Torch       |
