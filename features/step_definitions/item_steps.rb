Given(/^I created a new item without damage value$/) do
  @item = CraftingTable::Item.new('Stone', 1)
end

When(/^I query its damage value$/) do
  @result = @item.damage_value
end

Then(/^it should be 0$/) do
  expect(@result).to eq 0
end

Given(/^I have these items:$/) do |table|
  @items = table.raw[1..-1].map { |name, id, damage| CraftingTable::Item.new(name, id, damage) }
end

When(/^I query them for equality$/) do
  @results = @items.combination(2).map { |item_1, item_2| item_1 == item_2 }
end

Then(/^none of them should be equal$/) do
  expect(@results.any?).to be_false
end
