require 'pry'

Given(/^I have the following items loaded:$/) do |table|
  @manager = CraftingTable::ItemManager.new

  table.raw[1..-1].each do |name, id, damage_value|
    if damage_value.empty?
      damage_value = :any
    else
      damage_value = damage_value.to_i
    end

    @manager.add(CraftingTable::Item.new(name, id.to_i, damage_value))
  end
end


Given(/^I have the vanilla items loaded$/) do
  @item_manager = CraftingTable::ItemManager.new
  @item_manager.add_from_file ITEM_FILE
end


Given(/^(?:I )?use exact matching: (.*)$/) do |exact_match|
  @exact_match = string_to_boolean(exact_match)
end

Given(/^(?:I )?use case sensitivity: (.*)$/) do |case_sensitive|
  @case_sensitive = string_to_boolean(case_sensitive)
end

When(/^I search for its name: (.*)$/) do |name|
  @results = @manager.find do |search|
    search.name = name
    search.case_sensitive = @case_sensitive
    search.exact = @exact_match
  end
end

When(/^I search for item ID (.*)$/) do |item_id|
  @results = @manager.find do |search|
    search.item_id = string_to_range_or_integer(item_id)
  end
end

When(/^I search for damage value (.*)$/) do |damage_value|
  @results = @manager.find do |search|
    search.damage_value = string_to_range_or_integer(damage_value)
  end
end

When(/^I search for the name '([^']*)', using fuzzy matching, and for damage value (\d+)$/) do |name, damage|
  @results = @manager.find do |search|
    search.name = name
    search.damage_value = string_to_range_or_integer(damage)
    search.exact = false
  end
end

Then(/^it should return the following items: (.*)$/) do |item_names|
  expect(@results.map(&:name).sort).to eq (item_names.split(', ').sort)
end


Then(/^it should return "([^"]*)"$/) do |item_names|
  expect(@results.map(&:name).sort).to eq (item_names.split(', ').sort)
end

Given(/^I have a YAML file containing multiple items$/) do
  expect(File.exist? ITEM_FILE).to be_true
end

When(/^I tell the item manager to load the file$/) do
  @manager = CraftingTable::ItemManager.new
  @manager.add_from_file(ITEM_FILE)
end

Then(/^it should contain "([^"]*)" items$/) do |arg|
  expect(@manager).to have(arg.to_i).items
end

When(/^it should contain the following items: "([^"]*)"$/) do |arg|
  item_names = @manager.items.map(&:name)

  arg.split(', ').each do |item_name|
    expect(item_names).to include item_name
  end
end
