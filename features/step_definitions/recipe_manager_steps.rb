When(/^I have the vanilla recipes loaded$/) do
  @recipe_manager = CraftingTable::RecipeManager.new(@item_manager)
  @recipe_manager.add_from_file RECIPE_FILE
end

When(/^I search for the recipe's name: (.*)$/) do |name|
  @results = @recipe_manager.find do |search|
    search.name = name
    search.case_sensitive = @case_sensitive
    search.exact = @exact_match
  end
end

Then(/^the result should contain the following recipe: (.*)$/) do |recipe_name|
  if recipe_name.empty?
    expect(@results).to be_empty
  else
    expect(@results.map(&:name)).to include recipe_name
  end
end


Given(/^I want (\d+) of torches$/) do |amount_torch|
  @amount = amount_torch.to_i
end

When(/^I resolve the recipe$/) do
  recipe_torch =  @recipe_manager.find_by_name('Torch').first
  @result = @recipe_manager.resolve_recipe(recipe_torch, @amount)
end

Then(/^it should require (\d+) of wood$/) do |amount_wood|
  item_wood = @item_manager.find_by_name('Wood').select { |item| item.damage_value == 0 }.first

  expect(@result[item_wood]).to eq amount_wood.to_i
end

Then(/^it should require (\d+) of coal$/) do |amount_coal|
  item_coal = @item_manager.find_by_name('Coal').first
  
  expect(@result[item_coal]).to eq amount_coal.to_i
end
