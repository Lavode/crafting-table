Feature: Searching items by various criteria
  In order to easily retrieve items
  As a developer
  I want to be able to search for items based on various criteria.

  Background:
    Given I have the following items loaded:
      | Name        | Item ID | Damage       |
      | Stone       | 1       |              |
      | Dirt        | 3       |              |
      | Sand        | 12      |              |
      | Gold Ore    | 14      |              |
      | Iron Ore    | 15      |              |
      | Coal Ore    | 16      |              |
      | Oak Wood    | 17      | 0            |
      | Spruce Wood | 17      | 1            |
      | Birch Wood  | 17      | 2            |
      | Jungle Wood | 17      | 3            |
      | Sandstone   | 24      |              |

  Scenario Outline: Searching for item's name
    Given I use exact matching: <exact_match>
    And use case sensitivity: <case_sensitive>
    When I search for its name: <name>
    Then it should return the following items: <item_names>

    Examples: Case-sensitive, exact matching
      | name     | exact_match | case_sensitive | item_names                                  |
      | Stone    | yes         | yes            | Stone                                       |
      | Gold Ore | yes         | yes            | Gold Ore                                    |
      | Ore      | yes         | yes            |                                             |
      | stone    | yes         | yes            |                                             |

    Examples: Case-insensitive, exact matching
      | name     | exact_match | case_sensitive | item_names                                  |
      | Stone    | yes         | no             | Stone                                       |
      | Gold Ore | yes         | no             | Gold Ore                                    |
      | Ore      | yes         | no             |                                             |
      | stone    | yes         | no             | Stone                                       |

    Examples: Case-sensitive, non-exact matching
      | name     | exact_match | case_sensitive | item_names                                  |
      | Stone    | no          | yes            | Stone                                       |
      | Gold Ore | no          | yes            | Gold Ore                                    |
      | Ore      | no          | yes            | Gold Ore, Iron Ore, Coal Ore                |
      | iron ore | no          | yes            |                                             |

    Examples: Case-insensitive, non-exact matching
      | name     | exact_match | case_sensitive | item_names                                  |
      | Stone    | no          | no             | Stone, Sandstone                            |
      | Gold Ore | no          | no             | Gold Ore                                    |
      | Ore      | no          | no             | Gold Ore, Iron Ore, Coal Ore                |
      | stone    | no          | no             | Stone, Sandstone                            |


  Scenario Outline: Searching for item's ID
    When I search for item ID <id>
    Then it should return the following items: <item_names>

    Examples: Using an integer
      | id | item_names                                     |
      | 1  | Stone                                          |
      | 17 | Oak Wood, Spruce Wood, Birch Wood, Jungle Wood |

    Examples: Using a range
      | id    | item_names           |
      | 3..14 | Dirt, Sand, Gold Ore |

  Scenario Outline: Searching for item's damage value
    When I search for damage value <damage_value>
    Then it should return the following items: <item_names>

    Examples: Using an integer
      | damage_value | item_names              |
      |            2 | Birch Wood              |
      |            3 | Jungle Wood             |
      |         2..3 | Birch Wood, Jungle Wood |

  Scenario: Searching for multiple conditions
    When I search for the name 'Wood', using fuzzy matching, and for damage value 3
    Then it should return the following items: Jungle Wood


