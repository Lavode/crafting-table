Feature: Differentiating between several versions of an item
  In order to distinguish between several versions of an item, like several types of wood, or damaged and undamaged tools
  As a developer
  I want to be able to specify a meta value of an item.

  Scenario: Damage value defaults to 0
    Given I created a new item without damage value
    When I query its damage value
    Then it should be 0

  Scenario: Same base items with different damage values are not equal
    Given I have these items:
      | name | item_id | damage_value |
      | Axe  | 123     | 0            |
      | Axe  | 123     | 1            |

    When I query them for equality
    Then none of them should be equal
