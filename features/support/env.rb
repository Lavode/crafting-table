#coding: utf-8

require_relative '../../lib/crafting_table'

ITEM_FILE = 'data/test/items/vanilla.yml'
RECIPE_FILE = 'data/test/recipes/vanilla.yml'

def string_to_boolean(string)

  if string.downcase =~ /yes|y/
    true
  elsif string.downcase =~ /no|n/
    false
  else
    raise ArgumentError
  end

end

def string_to_range_or_integer(string)
  if string =~ /^[0-9]+$/
    string.to_i
  elsif string =~ /^[0-9]+\.\.[0-9]+$/
    Range.new(*string.split('..').map(&:to_i))
  else
    fail "Could not convert #{ string.inspect } to range or integer."
  end
end

