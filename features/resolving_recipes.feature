Feature: Resolving recipes into their base components
  In order to help me with crafting
  As a user
  I want to be able to resolve recipes into their base components.

  Background:
    Given I have the vanilla items loaded
    And I have the vanilla recipes loaded

  Scenario Outline: Resolving the torch recipe
    Given I want <amount_torch> of torches
    When I resolve the recipe
    Then it should require <amount_wood> of wood
    And it should require <amount_coal> of coal

  # 1 Wood => 4 Planks => 8 Sticks => 32 Torches
  # 1 Coal => 4 Torches
  Examples: Resolving the torch recipe
    | amount_torch | amount_wood | amount_coal |
    |            1 |           1 |           1 |
    |            4 |           1 |           1 |
    |           16 |           1 |           4 |
    |           17 |           1 |           5 |
    |           32 |           1 |           8 |
    |           33 |           2 |           9 |
    |          100 |           4 |          25 |
