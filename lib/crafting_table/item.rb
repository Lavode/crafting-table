#coding: utf-8

module CraftingTable

  # A class representing a single item.
  # An item, in this case, can be any block, tool, loot etc.
  #
  # @author Michael Senn <morrolan@morrolan.ch>
  # @since 0.1
  class Item
    attr_reader :name, :item_id, :damage_value

    # Create a new Item.
    #
    # @example Spruce Wood
    #   item = Item.new('Spruce Wood', 17, 1)
    #
    # @example Glass
    #   item = Item.new('Glass', 20, :any)
    #
    # @param [String] name The item's name.
    # @param [Integer, Symbol] damage_value The item's damage / meta value.
    # @param [Integer] item_id The item's ID.
    def initialize(name, item_id, damage_value = 0)
      @name, @item_id, @damage_value = name, item_id, damage_value
    end

    # Compare two items for equality.
    #
    # Two items are considered equal, if their name, item ID and damage value are equal.
    #
    # @param [Item] other Item which to compare for equality.
    # @return [Boolean] Whether the two items are equal.
    def ==(other)
      name == other.name && item_id == other.item_id && damage_value == other.damage_value
    end

    alias_method(:eql?, :==)

    # Return reasonable hash value of this item.
    #
    # @since 0.2
    # 
    # @return [Integer] Hash of item's name, id and damage value.
    def hash
      [name, item_id, damage_value].hash
    end

    # Return a unique identifier of this item.
    # The identifier is an array, containing its ID and damage value
    #
    # @since 0.2
    #
    # @return [Array<Integer>] Unique identifier of this item.
    def identifier
      [item_id, damage_value]
    end
    
  end

end
