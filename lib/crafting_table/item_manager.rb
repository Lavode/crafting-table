#coding: utf-8

require 'yaml'

module CraftingTable

  # A class which contains items, and allows to search through them.
  #
  # @author Michael Senn <morrolan@morrolan.ch>
  # @since 0.1
  class ItemManager
    attr_reader :items

    # Create a new ItemManager
    #
    # @param [Array] items Array of items with which to initialize the item manager.
    def initialize(items = [])
      @items = items.to_ary
    end

    # Add a new item to the internal collection.
    #
    # @param [Item] item Item which to add to the collection.
    # @return [void]
    def add(item)
      @items << item
    end

    # Add new items by reading them from a YAML file.
    #
    # @param [String] path The path to the file from which to read items from.
    # @return [void]
    def add_from_file(path)
      YAML.load_file(path).each do |hash|
        add(Item.new(hash['name'], hash['id'], hash['damage']))
      end
    end

    # Clear the internal collection of items.
    # @return [void]
    def clear
      @items.clear
    end

    # Find items by their name.
    #
    # @deprecated Use {#find} instead
    #
    # @example Search using default parameters.
    #   manager.find_by_name('Stone').map(&:name) #=> ['Stone']
    #
    # @example Case-insensitive search, non-exact matching.
    #   manager.find_by_name('stone', exact: false, case_sensitive: false).map(&:name) #=> ['Stone', 'Sandstone', '...']
    #
    # @param [String] name The name for which to search.
    # @param [Hash] options Options which influence the search.
    # @option options [Boolean] :exact (true) Whether to match names exactly.
    # @option options [Boolean] :case_sensitive (true) Whether to search case-sensitively.
    # @return [Array<Item>] Collection of items which matched the search condition.
    def find_by_name(name, options = {})
      default_options = { exact: true, case_sensitive: true }
      default_options.update(options)

      if default_options[:case_sensitive]
        if default_options[:exact]
          items.select { |item| item.name == name }
        else
          items.select { |item| item.name.include? name }
        end
      else
        if default_options[:exact]
          items.select { |item| item.name.downcase == name.downcase }
        else
          items.select { |item| item.name.downcase.include? name.downcase }
        end
      end
    end

    # Find items by their ID.
    #
    # @deprecated Use {#find} instead
    #
    # @example Searching for single ID.
    #   manager.find_by_item_id(17).first.name #=> 'Wood'
    #
    # @example Searching for range of IDs.
    #   manager.find_by_item_id(14..16).map(&:name) #=> ['Gold Ore', 'Iron Ore', 'Coal Ore']
    #
    # @example Searching for collection of IDs.
    #   manager.find_by_item_id([1, 3, 24]).map(&:name) #=> ['Stone', 'Dirt', 'Sandstone']
    #
    # @param [#include?, Integer] id A collection of IDs, or a single ID, for which to search.
    # @return [Array<Item>] Collection of items which matched the search condition.
    def find_by_item_id(id)
      if id.respond_to? :include?
        items.select { |item| id.include? item.item_id }
      else
        items.select { |item| item.item_id == id }
      end
    end

    # Find items.
    #
    # @since 0.3
    #
    # @example Searching for name and damage value
    #   results = manager.find do |search|
    #     search.name = 'Wood'
    #     search.exact = false
    #     search.damage_value = 3
    #    end
    #    results.first.name #=> 'Jungle Wood'
    #
    # @example Searching for range of item IDs and two specific damage values
    #   results = manager.find do |search|
    #     search.item_id = 1..20
    #     search.damage_value = [1, 3]
    #   end
    #   results.map(&:name) #=> ['Spruce Sapling', 'Jungle Sapling', 'Spruce Wood', 'Jungle Wood', 'Spruce Leaves', 'Jungle Leaves']
    #
    # @yieldparam builder [SearchBuilder]
    #   An instance of the SearchBuilder class which allows to easily specify
    #   multiple search conditions.
    #
    # @return [Array<Item>] Items which matched the search conditions.
    def find(&block)
      builder = Search::SearchBuilder.new
      yield builder
      
      builder.searches.inject(items) { |items, search| search.apply_to(items) }
    end

    # Find items by their damage value.
    #
    # @deprecated Use {#find} instead
    #
    # @example Searching for single damage value.
    #   manager.find_by_damage_value(2).map(&:name) #=> ['Birch Wood', '...']
    #
    # @example Searching for range of damage values.
    #   manager.find_by_damage_value(1..2).map(&:name) #=> ['Spruce Wood', 'Birch Wood', '...']
    #
    # @example Searching for collection of damage values.
    #   manager.find_by_damage_value([1, 3]).map(&:name) #=> ['Spruce Wood', 'Jungle Wood', '...']
    #
    # @param [#include?, Integer] id A collection of damage values, or a single damage value, for which to search.
    # @return [Array<Item>] Collection of items which matched the search condition.
    def find_by_damage_value(damage)
      if damage.is_a?(Symbol) || !damage.respond_to?(:include?)
        items.select { |item| item.damage_value == damage }
      else
        items.select { |item| damage.include? item.damage_value }
      end
    end

    # Find items by their identifier.
    #
    # @see Item#identifier
    # @since 0.2
    #
    # @param [Array<Integer>] identifier Identifier for which to
    #search.
    # @return [Array<Item>] Collection of items which matched
    #   the search condition.
    def find_by_identifier(identifier)
      items.select { |item| item.identifier == identifier }
    end

  end

end
