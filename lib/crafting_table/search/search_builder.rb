#coding: utf-8

module CraftingTable

  module Search

    # A class which provides a basic API to construct searches.
    #
    # This allows to easily create multiple searches without having to
    # know about the underlying classes.
    #
    # @example Searching for name and damage value
    #   builder = SearchBuilder.new
    #   builder.name           = 'Wood'
    #   builder.exact          = false
    #   builder.case_sensitive = false
    #   builder.damage_value   = 3
    #
    #   builder.searches #=> [FuzzyNameSearch(...), DamageSearch(...)]
    #
    # @author Michael Senn <morrolan@morrolan.ch>
    # @since 0.3
    class SearchBuilder
      # @!attribute [rw] name
      #   @return [String] The name for which to search.
      #
      # @!attribute [rw] exact
      #   @return [Boolean] Whether to search for an exact match.
      #
      # @!attribute [rw] case_sensitive
      #   @return [Boolean] Whether to search case-sensitively.
      #
      # @!attribute [rw] damage_value
      #   @return [#include?, Fixnum]
      #     The damage value(s) for which to search.
      #
      # @!attribute [rw] item_id
      #   @return [#include?, Fixnum]
      #     The item ID(s) for which to search.
      #
      # @!attribute [rw] input
      #   @return [Item] The input for which to search.
      #
      # @!attribute [rw] output
      #   @return [Item] The output for which to search.      
      
      attr_accessor :name, :exact, :case_sensitive
      attr_accessor :damage_value
      attr_accessor :item_id
      attr_accessor :input, :output

      
      # Create a collection of searches based on the instance
      # variables which were set.
      #
      # @return [Array<*Search>]
      def searches
        searches = []

        if name
          c_sens = case_sensitive.nil? ? true : case_sensitive
          if exact
            searches << NameSearch.new(name, case_sensitive: c_sens)
          else
            searches << FuzzyNameSearch.new(name, case_sensitive: c_sens)
          end
        end

        searches << DamageSearch.new(damage_value) if damage_value

        searches << ItemIDSearch.new(item_id) if item_id

        searches << InputSearch.new(input) if input

        searches << OutputSearch.new(output) if output

        searches
        
      end

    end

  end

end
