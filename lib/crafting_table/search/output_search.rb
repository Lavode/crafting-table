#coding: utf-8

module CraftingTable

  module Search

    # A class which allows to filter recipes by their output.
    #
    # @author Michael Senn <morrolan@morrolan.ch>
    # @since 0.3
    class OutputSearch
      attr_reader :item

      # Create a new OutputSearch.
      #
      # @param [Item] item Item for which to filter.
      def initialize(item)
        @item = item
      end

      # Apply this filter to a collection of recipes.
      #
      # @param [Array<Recipe>] recipes
      #   Collection of recipes which to filter.
      # @return [Array<Recipe>]
      #   Recipes which matched the search criteria
      def apply_to(recipes)
        recipes.select { |recipe| recipe.output.include? item }
      end

      # Compare two searches for equality.
      #
      # They are considered equal if the item for which they filter is equal.
      #
      # @param [InputSearch] other InputSearch which to compare for equality.
      # @return [Boolean] Whether two searches are equal.
      def ==(other)
        other.item == item
      end

      alias_method :eq?, :==

    end

  end

end
