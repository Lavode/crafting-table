#coding: utf-8

module CraftingTable

  module Search

    # A class which allows to filter recipes and items by their name.
    #
    # @author Michael Senn <morrolan@morrolan.ch>
    # @since 0.3
    class NameSearch
      attr_reader :name, :case_sensitive

      # Create a new NameSearch
      #
      # @param [String] name Name for which to filter.
      # @param [Hash] options Options hash which influences the filtering.
      # @option options [Boolean] :case_sensitive (true)
      #   Whether to filter case-sensitively.
      def initialize(name, options = {})
        @name = name
        @case_sensitive = options.fetch(:case_sensitive, true)
      end

      def exact?
        true
      end

      def case_sensitive?
        @case_sensitive
      end

      # Apply this filter to a collection of items or recipes.
      # 
      # @param [Array<Item, Recipe>] collection
      #   Collection of items and recipes which to filter.
      # @return [Array<Item, Recipe>]
      #   Items and recipes which matched the search criteria.
      def apply_to(collection)
        if case_sensitive?
          if exact?
            collection.select { |item| item.name == name }
          else
            collection.select { |item| item.name.include? name }
          end
        else
          if exact?
            collection.select { |item| item.name.downcase == name.downcase }
          else
            collection.select { |item| item.name.downcase.include? name.downcase }
          end
        end
      end

      # Compare two searches for equality.
      #
      # They are considered equal if the name for which they filter is equal.
      #
      # @param [NameSearch] other NameSearch which to compare for equality.
      # @return [Boolean] Whether two searches are equal.
      def ==(other)
        other.name == name && other.case_sensitive == case_sensitive
      end

      alias_method :eq?, :==

    end
    
  end

end
