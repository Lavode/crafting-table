#coding: utf-8

module CraftingTable

  module Search

    # A class which allows to filter recipes and items by their item ID.
    #
    # @author Michael Senn <morrolan@morrolan.ch>
    # @since 0.3
    class ItemIDSearch
      attr_reader :item_id

      # Create a new ItemIDSearch
      #
      # @param [Fixnum, Range] item_id Item ID for which to search.
      def initialize(item_id)
        @item_id = item_id
      end

      # Apply this filter to a collection of items.
      #
      # @param [Array<Item>] items
      #   Collection of items which to filter.
      # @return [Array<Item>]
      #   Items which matched the search criteria
      def apply_to(items)
        if item_id.respond_to?(:include?)
          items.select { |item| item_id.include? item.item_id }
        else
          items.select { |item| item_id == item.item_id }
        end
      end

      # Compare two searches for equality.
      #
      # They are considered equal if the item ID for which they filter is equal.
      #
      # @param [ItemIDSearch] other ItemIDSearch which to compare for equality.
      # @return [Boolean] Whether two searches are equal.
      def ==(other)
        other.item_id == item_id
      end

      alias_method :eq?, :==

    end

  end

end
