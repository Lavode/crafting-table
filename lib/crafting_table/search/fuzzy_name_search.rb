#coding: utf-8

module CraftingTable
  
  module Search
    
    # A class which allows to filter recipes and items by their name.
    #
    # Unlike NameSearch, this class does not filter for exact matches,
    # but for inclusions. E.g. searching for "Ore" will return "Iron
    # Ore" as well as "Gold Ore".
    #
    # @author Michael Senn <morrolan@morrolan.ch>
    # @since 0.3
    class FuzzyNameSearch < NameSearch

      def exact?
        false
      end

      # Apply this filter to a collection of items or recipes.
      # 
      # @param [Array<Item, Recipe>] collection
      #   Collection of items and recipes which to filter.
      # @return [Array<Item, Recipe>]
      #   Items and recipes which matched the search criteria.
      def apply_to(collection)
        if case_sensitive?
          collection.select { |item| item.name.include? name }
        else
          collection.select { |item| item.name.downcase.include? name.downcase }
        end
      end
      
    end

  end

end
