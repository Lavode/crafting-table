#coding: utf-8

module CraftingTable

  module Search

    # A class which allows to filter recipes and items by their damage value.
    #
    # @author Michael Senn <morrolan@morrolan.ch>
    # @since 0.3
    class DamageSearch
      attr_reader :damage_value

      # Create a new DamageSearch
      #
      # @param [Fixnum, Range] damage_value Damage value for which to filter.
      def initialize(damage_value)
        @damage_value = damage_value
      end

      # Apply this filter to a collection of items.
      #
      # @param [Array<Item>] items
      #   Collection of items which to filter.
      # @return [Array<Item>]
      #   Items which matched the search criteria
      def apply_to(items)
        if damage_value.respond_to?(:include?)
          items.select { |item| damage_value.include? item.damage_value }
        else
          items.select { |item| damage_value == item.damage_value }
        end
      end

      # Compare two searches for equality.
      #
      # They are considered equal if the damage value for which they filter is equal.
      #
      # @param [DamageSearch] other DamageSearch which to compare for equality.
      # @return [Boolean] Whether two searches are equal.
      def ==(other)
        other.damage_value == damage_value
      end

      alias_method :eq?, :==
        
    end

  end

end
