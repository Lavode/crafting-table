#coding: utf-8

module CraftingTable

  # A class representing a single recipe.
  # A recipe has a name, one to n inputs, and one to n outputs.
  #
  # @author Michael Senn <morrolan@morrolan.ch>
  # @since 0.2
  class Recipe
    attr_reader :name, :input, :output

    # Create a new recipe.
    #
    # @example Torch
    #   recipe = Recipe.new('Torch',
    #                       { Item.new('Coal', 263, 0) => 1,
    #                         Item.new('Stick', 280, 0) => 1 },
    #                       { Item.new('Torch', 50, 0) => 4 })
    #
    # @param [String] name The recipe's name
    # @param [Hash{Item => Integer}] input
    #   A hash of items and their amounts which are required to craft
    #   the recipe.
    # @param [Hash{Item => Integer}] output
    #   A hash of items and their amounts which you get when crafting
    #   the recipe.
    def initialize(name, input, output)
      @name, @input, @output = name, input, output
    end

  end

end


