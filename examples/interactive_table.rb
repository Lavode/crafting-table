#!/usr/bin/env ruby
#coding: utf-8

require 'crafting_table'

class InteractiveTable
  attr_reader :item_manager, :recipe_manager
  
  def initialize(opts)
    @item_manager = CraftingTable::ItemManager.new
    @recipe_manager = CraftingTable::RecipeManager.new(item_manager)

    opts.fetch(:items, []).each do |file|
      @item_manager.add_from_file(file)
    end

    opts.fetch(:recipes, []).each do |file|
      @recipe_manager.add_from_file(file)
    end
  end

  def run
    loop do
      input = gets.chomp

      exit(0) if ['exit', 'close'].include? input
      
      handle(input)
    end
  end

  def item_search(cmd)
    cmd.match(/item (.*)/) do |match|
      name = match[1].strip
      results = item_manager.find_by_name(name, exact: false, case_sensitive: false)

      if results.empty?
        puts 'No results found'
      else
        puts results.map { |item| "- #{ item.name } (#{ item.item_id }:#{ item.damage_value })" }.join("\n")
      end
    end
  end
  
  def recipe_search(cmd)
    cmd.match(/recipe (.*)/) do |match|
      name = match[1].strip
      results = recipe_manager.find_by_name(name, exact: false, case_sensitive: false).each_with_index.to_a
      if results.empty?
        puts 'No results found'
        return
      else
        puts results.map { |recipe, index| "[#{ index }] #{ recipe.name }" }.join("\n")
        print 'Select recipe: '
        index = gets.chomp
        return if index == 'exit'
        recipe = results[index.to_i].first
        print 'Amount: '
        amount = gets.chomp.to_i
        components = recipe_manager.resolve_recipe(recipe, amount)
        puts "To craft #{ amount } of #{ recipe.name } you need:"
        puts components.map { |item, amount| "- #{ amount } of #{ item.name }" }.join("\n")
      end
    end
    
  end
  
  def usage
    puts 'Commands:'
    puts '`item <item_name>` - Search for items.'
    puts '`recipe <recipe_name>` - Search for, and resolve. recipes.'
  end
  
  def handle(cmd)
    if cmd.start_with?('item')
      item_search(cmd)
    elsif cmd.start_with?('recipe')
      recipe_search(cmd)
    else
      usage
    end
  end

end

def main
  
  table = InteractiveTable.new(:items => ['../data/items/vanilla.yml',
                                          '../data/items/thermal_expansion.yml'],
                               :recipes => ['../data/recipes/vanilla.yml',
                                            '../data/recipes/thermal_expansion.yml'])

  table.run
  
end

main

