#coding: utf-8

require_relative '../spec_helper'

module CraftingTable

  describe Item do

    describe '#initialize' do

      it 'should have the damage value default to 0' do
        expect(Item.new('Wood', 17).damage_value).to eq 0
      end

      it 'should set the appropriate instance variables' do
        comp = Item.new('Wood', 17, 1)
        expect(comp.name).to eq 'Wood'
        expect(comp.item_id).to eq 17
        expect(comp.damage_value).to eq 1
      end

    end

    describe '#==' do

      it 'should return true if name, item id and damage value are equal' do
        expect(Item.new('Wood', 17)).to eq Item.new('Wood', 17)
        expect(Item.new('Wood', 17, 2)).to eq Item.new('Wood', 17, 2)
      end

      it 'should return false if name or item id are not equal' do
        expect(Item.new('Wood', 17)).not_to eq Item.new('Wood', 16)
        expect(Item.new('Wood', 17)).not_to eq Item.new('Log', 17)
        expect(Item.new('Wood', 17, 1)).not_to eq Item.new('Wood', 17, 2)
      end

    end

    describe'#eql?' do

      it 'should behave like #==' do
        expect(Item.new('Wood', 17)).to eql Item.new('Wood', 17)
        expect(Item.new('Wood', 17, 2)).to eql Item.new('Wood', 17, 2)        
      end

    end

    describe '#hash' do

      it 'should return a proper value' do
        expect(Item.new('Wood', 17, 2).hash).to eq ['Wood', 17, 2].hash
      end

    end

    describe '#identifier' do

      it 'should return an array containing its id and damage value' do
        expect(Item.new('Wood', 17, 2).identifier).to eq [17, 2]
        expect(Item.new('Stone', 1, 0).identifier).to eq [1, 0]
      end

    end

  end

end
