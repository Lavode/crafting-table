#coding: utf-8

require_relative '../spec_helper'

module CraftingTable

  describe RecipeManager do
    let(:item_log) { Item.new('Log', 17) }

    let(:item_gold_ore) { Item.new('Gold Ore', 14) }
    let(:item_iron_ore) { Item.new('Iron Ore', 15) }

    let(:item_wood_oak) { Item.new('Oak Wood', 17, 0) }
    let(:item_wood_spruce) { Item.new('Spruce Wood', 17, 1) }
    let(:item_wood_birch) { Item.new('Birch Wood', 17, 2) }
    let(:item_wood_jungle) { Item.new('Jungle Wood', 17, 3) }

    let(:item_planks) { Item.new('Wood Planks', 5) }
    let(:item_coal) { Item.new('Coal', 263) }
    let(:item_stick) { Item.new('Stick', 280) }
    let(:item_torch) { Item.new('Torch', 50) }

    let(:item_manager) { ItemManager.new([item_log, item_planks, item_stick, item_coal, item_torch]) }
    
    let(:recipe_planks) { Recipe.new('Wood Planks', { item_wood_oak => 1 }, { item_planks => 4 }) }
    let(:recipe_sticks) { Recipe.new('Sticks', { item_planks => 2 }, { item_stick => 4 }) }
    let(:recipe_torch) { Recipe.new('Torch', { item_stick => 1, item_coal => 1 }, { item_torch => 4 }) }
    
    let(:manager) { RecipeManager.new(item_manager) }


    describe '#initialize' do

      it 'should set the appropriate instance variables' do
        expect(manager.recipes).to eq []
        expect(manager.item_manager).to eq item_manager
      end

    end

    describe '#add' do

      it 'should add the recipe to the internal collection' do
        expect { manager.add(recipe_planks) }.to change { manager.recipes.count }.by 1
        expect(manager.recipes).to include recipe_planks
      end
    end


    describe '#add_from_file' do

      it 'should add all recipes from the file' do
        item_manager = ItemManager.new
        item_manager.add_from_file(ITEM_FILE)
        
        manager = RecipeManager.new(item_manager)
        manager.add_from_file(RECIPE_FILE)

        expect(manager).to have(4).recipes
        recipe_names = manager.recipes.map(&:name).sort
        expect(recipe_names).to eq ['Sticks', 'Stone', 'Torch', 'Oak Wood Planks'].sort
      end
      
    end

    describe '#clear' do

      it 'should remove all recipes from the internal collection' do
        manager.add(recipe_torch)
        expect { manager.clear }.to change { manager.recipes.count }.to 0
      end

    end

    describe '#find' do
      before(:each) do
        manager.clear
        manager.add(recipe_torch)
        manager.add(recipe_planks)
        manager.add(recipe_sticks)
      end

      context "when searching for the recipe's name" do

        context 'with case sensitivity' do

          it 'should return recipes with matching case' do
            results = manager.find do |search|
              search.case_sensitive = true
              search.name = 'Torch'
            end
            expect(results).to eq [recipe_torch]

            results = manager.find do |search|
              search.case_sensitive = true
              search.name = 'Sticks'
            end
            expect(results).to eq [recipe_sticks]
          end

          it 'should not return recipes where the case does not match' do
            results = manager.find do |search|
              search.case_sensitive = true
              search.name = 'torch'
            end
            expect(results).to be_empty

            results = manager.find do |search|
              search.case_sensitive = true
              search.name = 'stICkS'
            end
            expect(results).to be_empty
          end

        end

        context 'with case insensitivity' do

          it 'should return recipes no matter their case' do
            results = manager.find do |search|
              search.case_sensitive = false
              search.name = 'Torch'
            end
            expect(results).to eq [recipe_torch]

            results = manager.find do |search|
              search.case_sensitive = false
              search.name = 'toRcH'
            end
            expect(results).to eq [recipe_torch]
          end
          
        end

        context 'with exact matching' do

          it 'should return exact matches' do
            results = manager.find do |search|
              search.exact = true
              search.name = 'Torch'
            end
            expect(results).to eq [recipe_torch]

            results = manager.find do |search|
              search.exact = true
              search.name = 'Sticks'
            end
            expect(results).to eq [recipe_sticks]
          end

          it 'should not return non-exact matches' do
            results = manager.find do |search|
              search.exact = true
              search.name = 'Tor'
            end
            expect(results).to be_empty

            results = manager.find do |search|
              search.exact = true
              search.name = 'ticks'
            end
            expect(results).to be_empty
          end

        end

        context 'with fuzzy matching' do

          it 'should return non-exact matches too' do
            results = manager.find do |search|
              search.exact = false
              search.name = 'Tor'
            end
            expect(results).to eq [recipe_torch]

            results = manager.find do |search|
              search.exact = false
              search.name = 'ticks'
            end
            expect(results).to eq [recipe_sticks]
          end

        end

      end

      context 'when searching for inputs' do

        it 'should return all recipes which have the item as input' do
          results = manager.find do |search|
            search.input = item_coal
          end
          expect(results).to eq [recipe_torch]

          results = manager.find do |search|
            search.input = item_wood_oak
          end
          expect(results).to eq [recipe_planks]
        end
        
      end

      context 'when searching for outputs' do

        it 'should return all recipes which have the item as output' do
          results = manager.find do |search|
            search.output = item_torch
          end
          expect(results).to eq [recipe_torch]

          results = manager.find do |search|
            search.output = item_planks
          end
          expect(results).to eq [recipe_planks]
        end
        
      end
      
    end

    describe '#find_by_name' do
      before(:each) do
        manager.clear
        manager.add(recipe_torch)
        manager.add(recipe_planks)
        manager.add(recipe_sticks)
      end

      context 'with case sensitivity' do
        let(:args) { { case_sensitive: true } }

        it 'should return recipes with matching case' do
          expect(manager.find_by_name('Torch', args).first).to eq recipe_torch
          expect(manager.find_by_name('Sticks', args).first).to eq recipe_sticks
        end

        it 'should not return recipes where the case does not match' do
          expect(manager.find_by_name('torch', args)).to be_empty
          expect(manager.find_by_name('stICkS', args)).to be_empty
        end

      end

      context 'with case-insensitivity' do
        let(:args) { { case_sensitive: false } }

        it 'should return recipes no matter their case' do
          expect(manager.find_by_name('Torch', args).first).to eq recipe_torch
          expect(manager.find_by_name('toRcH', args).first).to eq recipe_torch
        end

      end

      context 'with exact matching' do
        let(:args) { { exact: true } }

        it 'should return exact matches' do
          expect(manager.find_by_name('Torch', args).first).to eq recipe_torch
          expect(manager.find_by_name('Sticks', args).first).to eq recipe_sticks
        end

        it 'should not return non-exact matches' do
          expect(manager.find_by_name('Tor', args)).to be_empty
          expect(manager.find_by_name('ticks', args)).to be_empty
        end

      end

      context 'with fuzzy matching' do
        let(:args) { { exact: false } }

        it 'should return non-exact matches too' do
          expect(manager.find_by_name('Tor', args).first).to eq recipe_torch
          expect(manager.find_by_name('ticks', args).first).to eq recipe_sticks
        end

      end

    end

    describe '#find_by_input' do
      before(:each) do
        manager.clear
        manager.add(recipe_torch)
        manager.add(recipe_planks)
        manager.add(recipe_sticks)
      end

      it 'should return all recipes which have the item as input' do
        expect(manager.find_by_input(item_coal).first).to eq recipe_torch
        expect(manager.find_by_input(item_wood_oak).first).to eq recipe_planks
      end

    end

    describe '#find_by_output' do
      before(:each) do
        manager.clear
        manager.add(recipe_torch)
        manager.add(recipe_planks)
        manager.add(recipe_sticks)
      end

      it 'should return all recipes which have the item as output' do
        expect(manager.find_by_output(item_torch).first).to eq recipe_torch
        expect(manager.find_by_output(item_planks).first).to eq recipe_planks
      end

    end

    describe '#resolve_recipe' do
      before(:each) do
        manager.clear
        manager.add(recipe_torch)
        manager.add(recipe_planks)
        manager.add(recipe_sticks)
      end

      context 'when resolving one torch' do
        let(:result) { manager.resolve_recipe(recipe_torch, 1) }

        it 'should return one wood and one coal' do
          expect(result[item_coal]).to eq 1
          expect(result[item_wood_oak]).to eq 1
        end

      end

      context 'when resolving 32 torches' do
        let(:result) { manager.resolve_recipe(recipe_torch, 32) }

        it 'should return one wood and 8 coal' do
          expect(result[item_coal]).to eq 8
          expect(result[item_wood_oak]).to eq 1
        end

      end

      context 'when resolving 100 torches' do
        let(:result) { manager.resolve_recipe(recipe_torch, 100) }

        it 'should return 4 wood and 25 coal' do
          expect(result[item_coal]).to eq 25
          expect(result[item_wood_oak]).to eq 4
        end

      end

    end
    
  end
  
end
