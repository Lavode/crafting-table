#coding: utf-8

require_relative '../spec_helper'

module CraftingTable

  describe ItemManager do
    let(:manager) { ItemManager.new }
    let(:item_log) { Item.new('Log', 17) }
    let(:item_gold_ore) { Item.new('Gold Ore', 14) }
    let(:item_iron_ore) { Item.new('Iron Ore', 15) }
    let(:item_stone) { Item.new('Stone', 1) }

    let(:item_wood_oak) { Item.new('Oak Wood', 17, 0) }
    let(:item_wood_spruce) { Item.new('Spruce Wood', 17, 1) }
    let(:item_wood_birch) { Item.new('Birch Wood', 17, 2) }
    let(:item_wood_jungle) { Item.new('Jungle Wood', 17, 3) }


    describe '#initialize' do

      it 'should set the appropriate instance variables' do
        expect(manager.items).to eq []

        expect(ItemManager.new([item_log, item_gold_ore]).items).to eq [item_log, item_gold_ore]
      end

    end

    describe '#add' do

      it 'should add the item to the internal collection' do
        expect { manager.add(item_log) }.to change { manager.items.count }.by 1
        expect(manager.items).to include item_log
      end

    end

    describe '#add_from_file' do

      it 'should add all items from file' do
        # Todo: This test is very brittle. A separate data file for
        # testing purposes which does not get modified might be a good idea.

        manager.add_from_file(ITEM_FILE)
        expect(manager.items).to have(371).items

        item_names = manager.items.map(&:name)
        ['Stone', 'Gold Ore', 'Piston', 'Lapis Lazuli Ore'].each do |name|
          expect(item_names).to include name
        end

        expect(manager.find_by_identifier([1, 0]).first.name).to eq 'Stone'
      end

    end

    describe '#clear' do

      it 'should remove all items from the internal collection' do
        manager.add(item_gold_ore)
        expect { manager.clear }.to change { manager.items.count }.to 0
      end

    end

    describe '#find' do
      before(:each) do
        manager.clear
        manager.add(item_log)
        manager.add(item_gold_ore)
        manager.add(item_iron_ore)
        manager.add(item_stone)

        manager.add(item_wood_oak)
        manager.add(item_wood_spruce)
        manager.add(item_wood_birch)
        manager.add(item_wood_jungle)        
      end

      context "when searching for the item's name" do

        context 'with case sensitivity' do

          it 'should return items with matching case' do
            results = manager.find do |search|
              search.case_sensitive = true
              search.name = 'Gold Ore'
            end
            expect(results).to eq [item_gold_ore]

            results = manager.find do |search|
              search.case_sensitive = true              
              search.name = 'Log'
            end
            expect(results).to eq [item_log]
          end

          it 'should not return items where the case does not match' do
            results = manager.find do |search|
              search.case_sensitive = true
              search.name = 'gold ore'
            end
            expect(results).to be_empty

            results = manager.find do |search|
              search.case_sensitive = true              
              search.name = 'LoG'
            end
            expect(results).to be_empty
          end

        end

        context 'with case-insensitivity' do

          it 'should return items no matter their case' do
            results = manager.find do |search|
              search.case_sensitive = false
              search.name = 'Gold Ore'
            end
            expect(results).to eq [item_gold_ore]

            results = manager.find do |search|
              search.case_sensitive = false
              search.name = 'LOG'
            end
            expect(results).to eq [item_log]

            results = manager.find do |search|
              search.case_sensitive = false
              search.name = 'iron ore'
            end
            expect(results).to eq [item_iron_ore]
          end

        end

        context 'with exact matching' do

          it 'should return exact matches' do
            results = manager.find do |search|
              search.exact = true
              search.name = 'Gold Ore'
            end
            expect(results).to eq [item_gold_ore]
          end

          it 'should not return non-exact matches' do
            results = manager.find do |search|
              search.exact = true
              search.name = 'Gold'
            end
            expect(results).to be_empty

            results = manager.find do |search|
              search.exact = true
              search.name = 'Ore'
            end
            expect(results).to be_empty
          end

        end

        context 'with fuzzy matching' do

          it 'should return non-exact matches too' do
            results = manager.find do |search|
              search.exact = false
              search.name = 'Gold Ore'
            end
            expect(results).to eq [item_gold_ore]

            results = manager.find do |search|
              search.exact = false
              search.name = 'Ore'
            end
            expect(results).to eq [item_gold_ore, item_iron_ore]
          end

        end

      end

      context "when searching for the item's damage value" do

        context 'with an integer' do

          it 'should return items with matching damage value' do
            results = manager.find do |search|
              search.damage_value = 3
            end
            expect(results).to eq [item_wood_jungle]

            results = manager.find do |search|
              search.damage_value = 1
            end
            expect(results).to eq [item_wood_spruce]
          end

        end

        context 'with a range or array' do

          it 'should return items the damage value of which is included in the range or array' do
            results = manager.find do |search|
              search.damage_value = 1..3
            end
            expect(results).to eq [item_wood_spruce,
                                   item_wood_birch,
                                   item_wood_jungle]

            results = manager.find do |search|
              search.damage_value = [1, 3]
            end
            expect(results).to eq [item_wood_spruce,
                                   item_wood_jungle]
          end

        end

      end

      context "when searching for the item's ID" do
        # let(:item_stone { Item.new('Stone', 1) }
        # let(:item_log) { Item.new('Log', 17) }
        # let(:item_gold_ore) { Item.new('Gold Ore', 14) }
        # let(:item_iron_ore) { Item.new('Iron Ore', 15) }


        context 'with an integer' do

          it 'should return items with matching ID' do
            results = manager.find do |search|
              search.item_id = 15
            end
            expect(results).to eq [item_iron_ore]

            results = manager.find do |search|
              search.item_id = 1
            end
            expect(results).to eq [item_stone]
          end

        end

        context 'with a range or array' do

          it 'should return items the item ID of which is included in the range or array' do
            results = manager.find do |search|
              search.item_id = 14..15
            end
            expect(results.sort_by { |item| item.name }).to eq [item_gold_ore,
                                                                item_iron_ore]

            results = manager.find do |search|
              search.item_id = [1, 15]
            end
            expect(results.sort_by { |item| item.name }).to eq [item_iron_ore,
                                                               item_stone]
          end

        end

      end

      context 'with multiple searches' do

        it 'should return items which matched all criteria' do
          results = manager.find do |search|
            search.name = 'Wood'
            search.exact = false

            search.damage_value = 3
          end
          expect(results).to eq [item_wood_jungle]
        end

      end

    end

    


    describe '#find_by_name' do
      before(:each) do
        manager.clear
        manager.add(item_log)
        manager.add(item_gold_ore)
        manager.add(item_iron_ore)
      end

      context 'with case sensitivity' do
        let(:args) { { case_sensitive: true } }

        it 'should return items with matching case' do
          expect(manager.find_by_name('Gold Ore', args).first).to eq item_gold_ore
          expect(manager.find_by_name('Log', args).first).to eq item_log
        end

        it 'should not return items where the case does not match' do
          expect(manager.find_by_name('gold ore', args)).to be_empty
          expect(manager.find_by_name('LoG', args)).to be_empty
        end

      end

      context 'with case-insensitivity' do
        let(:args) { { case_sensitive: false } }

        it 'should return items no matter their case' do
          expect(manager.find_by_name('Gold Ore', args).first).to eq item_gold_ore
          expect(manager.find_by_name('LOG', args).first).to eq item_log
          expect(manager.find_by_name('iron ore', args).first).to eq item_iron_ore
        end

      end

      context 'with exact matching' do
        let(:args) { { exact: true } }

        it 'should return exact matches' do
          expect(manager.find_by_name('Gold Ore', args).first).to eq item_gold_ore
        end

        it 'should not return non-exact matches' do
          expect(manager.find_by_name('Gold', args)).to be_empty
          expect(manager.find_by_name('Ore', args)).to be_empty
        end

      end

      context 'with fuzzy matching' do
        let(:args) { { exact: false } }

        it 'should return non-exact matches too' do
          expect(manager.find_by_name('Gold Ore', args).first).to eq item_gold_ore

          expect(manager.find_by_name('Gold', args).first).to eq item_gold_ore
          expect(manager.find_by_name('Ore', args).sort { |i1, i2| i1.name <=> i2.name } ) .to eq [item_gold_ore, item_iron_ore]
        end

      end

    end

    describe '#find_by_item_id' do
      before(:each) do
        manager.clear
        manager.add(item_log)
        manager.add(item_gold_ore)
        manager.add(item_iron_ore)
      end

      context 'when supplied an integer' do

        it 'should return all items with the same item id' do
          expect(manager.find_by_item_id(17).first).to eq item_log
          expect(manager.find_by_item_id(0)).to be_empty
        end

      end

      context 'when supplied something which provides #include?' do

        it 'should return all items the id of which was included in the collection' do
          expect(manager.find_by_item_id(14..15).sort { |i1, i2| i1.name <=> i2.name }).to eq [item_gold_ore, item_iron_ore]
          expect(manager.find_by_item_id([15, 17]).sort { |i1, i2| i1.name <=> i2.name }).to eq [item_iron_ore, item_log]
        end

      end

    end

    describe '#find_by_damage_value' do
      before(:each) do
        manager.clear
        manager.add(item_wood_oak)
        manager.add(item_wood_spruce)
        manager.add(item_wood_birch)
        manager.add(item_wood_jungle)
      end

      context 'when supplied an integer' do

        it 'should return all items with the same damage value' do
          expect(manager.find_by_damage_value(2).first).to eq item_wood_birch
        end

      end

      context 'when supplied something which provides #include?' do

        it 'should return all items the damage value of which was included in the collection' do
          expect(manager.find_by_damage_value(0..2).sort { |i1, i2| i1.damage_value <=> i2.damage_value }).to eq [item_wood_oak, item_wood_spruce, item_wood_birch]
          expect(manager.find_by_damage_value([1, 3]).sort { |i1, i2| i1.damage_value <=> i2.damage_value }).to eq [item_wood_spruce, item_wood_jungle]
        end

      end

    end

    describe '#find_by_identifier' do
      before(:each) do
        manager.clear
        manager.add(item_wood_oak)
        manager.add(item_wood_spruce)
        manager.add(item_wood_birch)
        manager.add(item_wood_jungle)
      end

      it 'should return all items with the specified identifier' do
        expect(manager.find_by_identifier([17, 1]).first).to eq item_wood_spruce
      end

    end

  end

end
