#coding: utf-8

require_relative '../spec_helper'

module CraftingTable

  describe Recipe do
    let(:comp_wood) { Item.new('Wood', 17, 0) }
    let(:comp_planks) { Item.new('Wood Planks', 5, 0) }

    let(:recipe_planks) { Recipe.new('Wood Planks', { comp_wood => 1 }, { comp_planks => 2 }) }
    
    describe '#initialize' do

      it 'should set the appropriate instance variables' do
        expect(recipe_planks.name).to eq 'Wood Planks'
        expect(recipe_planks.input).to eq({ comp_wood => 1 })
        expect(recipe_planks.output).to eq({ comp_planks => 2})
      end

    end

  end

end
