#coding: utf-8

require_relative '../../spec_helper'

module CraftingTable

  module Search
    
    describe NameSearch do

      describe '#initialize' do

        it 'should set the appropriate instance variables' do
          search = NameSearch.new('Gold Ore', case_sensitive: false)
          expect(search.name).to eq 'Gold Ore'
          expect(search).to_not be_case_sensitive
        end

        it 'should be exact' do
          search = NameSearch.new('Gold Ore', exact: true, case_sensitive: false)
          expect(search).to be_exact
        end
        
        it 'should default to case-sensitive matching' do
          search = NameSearch.new('Foobar')
          expect(search).to be_case_sensitive
        end

      end

      describe '#apply_to' do
        let(:item_log) { Item.new('Log', 17) }
        let(:item_gold_ore) { Item.new('Gold Ore', 14) }
        let(:item_iron_ore) { Item.new('Iron Ore', 15) }
        let(:items) { [item_log, item_gold_ore, item_iron_ore] }

        context 'with case sensitivity' do
          let(:args) { { case_sensitive: true } }

          it 'should return items with matching case' do
            expect(NameSearch.new('Gold Ore', args).apply_to(items)).to eq [item_gold_ore]
            expect(NameSearch.new('Log', args).apply_to(items)).to eq [item_log]
          end

          it 'should not return items where the case does not match' do
            expect(NameSearch.new('gold ore', args).apply_to(items)).to be_empty
          end

        end

        context 'with case-insensitivity' do
          let(:args) { { case_sensitive: false } }
          
          it 'should return items no matter their case' do
            expect(NameSearch.new('Gold Ore', args).apply_to(items)).to eq [item_gold_ore]
            expect(NameSearch.new('GOld OrE', args).apply_to(items)).to eq [item_gold_ore]
            expect(NameSearch.new('gold ore', args).apply_to(items)).to eq [item_gold_ore]
          end

        end

      end

      describe '#==' do

        it 'should return true if name and case sensitivity match' do
          expect(NameSearch.new('Gold Ore', case_sensitive: true)).to eq NameSearch.new('Gold Ore', case_sensitive: true)
          expect(NameSearch.new('Wood', case_sensitive: false)).to eq NameSearch.new('Wood', case_sensitive: false)
        end

        it "should return false if name or case sensitivity don't match" do
          expect(NameSearch.new('Stone', case_sensitive: true)).not_to eq NameSearch.new('Stone', case_sensitive: false)
          expect(NameSearch.new('Stone', case_sensitive: true)).not_to eq NameSearch.new('Stoone', case_sensitive: true)
        end

      end
      
    end

    describe FuzzyNameSearch do

      describe '#initialize' do

        it 'should set the appropriate instance variables' do
          search = FuzzyNameSearch.new('Gold Ore', case_sensitive: false)
          expect(search.name).to eq 'Gold Ore'
          expect(search).to_not be_case_sensitive
        end

        it 'should not be exact' do
          search = FuzzyNameSearch.new('Gold Ore', case_sensitive: false)
          expect(search).to_not be_exact
        end
        
        it 'should default to case-sensitive matching' do
          search = FuzzyNameSearch.new('Foobar')
          expect(search).to be_case_sensitive
        end

      end

      describe '#apply_to' do
        let(:item_log) { Item.new('Log', 17) }
        let(:item_gold_ore) { Item.new('Gold Ore', 14) }
        let(:item_iron_ore) { Item.new('Iron Ore', 15) }
        let(:items) { [item_log, item_gold_ore, item_iron_ore] }

        context 'with case sensitivity' do
          let(:args) { { case_sensitive: true } }

          it 'should return items with matching case' do
            expect(FuzzyNameSearch.new('Gold', args).apply_to(items)).to eq [item_gold_ore]
            expect(FuzzyNameSearch.new('og', args).apply_to(items)).to eq [item_log]
          end

          it 'should not return items where the case does not match' do
            expect(FuzzyNameSearch.new('gold', args).apply_to(items)).to be_empty
          end

        end

        context 'with case-insensitivity' do
          let(:args) { { case_sensitive: false } }
          
          it 'should return items no matter their case' do
            expect(FuzzyNameSearch.new('Gold', args).apply_to(items)).to eq [item_gold_ore]
            expect(FuzzyNameSearch.new('GOld', args).apply_to(items)).to eq [item_gold_ore]
            expect(FuzzyNameSearch.new('gold', args).apply_to(items)).to eq [item_gold_ore]
          end

        end

      end

      describe '#==' do

        it 'should return true if name and case sensitivity match' do
          expect(FuzzyNameSearch.new('Gold Ore', case_sensitive: true)).to eq FuzzyNameSearch.new('Gold Ore', case_sensitive: true)
          expect(FuzzyNameSearch.new('Wood', case_sensitive: false)).to eq FuzzyNameSearch.new('Wood', case_sensitive: false)
        end

        it "should return false if name or case sensitivity don't match" do
          expect(FuzzyNameSearch.new('Stone', case_sensitive: true)).not_to eq FuzzyNameSearch.new('Stone', case_sensitive: false)
          expect(FuzzyNameSearch.new('Stone', case_sensitive: true)).not_to eq FuzzyNameSearch.new('Stoone', case_sensitive: false)
        end

      end

    end

    describe DamageSearch do

      describe '#initialize' do

        it 'should set the appropriate instance variables' do
          expect(DamageSearch.new(1).damage_value).to eq 1
          expect(DamageSearch.new(2..4).damage_value).to eq 2..4
        end

      end

      describe '#apply_to' do
        let(:item_log) { Item.new('Log', 17) }
        let(:item_gold_ore) { Item.new('Gold Ore', 14) }
        let(:item_iron_ore) { Item.new('Iron Ore', 15) }

        let(:item_wood_oak) { Item.new('Oak Wood', 17, 0) }
        let(:item_wood_spruce) { Item.new('Spruce Wood', 17, 1) }
        let(:item_wood_birch) { Item.new('Birch Wood', 17, 2) }
        let(:item_wood_jungle) { Item.new('Jungle Wood', 17, 3) }
        
        let(:items) { [item_wood_oak, item_wood_spruce,
                       item_wood_birch, item_wood_jungle] }

        context 'with an integer' do

          it 'should return items with matching damage value' do
            expect(DamageSearch.new(3).apply_to(items)).to eq [item_wood_jungle]
            expect(DamageSearch.new(1).apply_to(items)).to eq [item_wood_spruce]
          end

        end

        context 'with a range or array' do

          it 'should return items the damage value of which is included in the range or array' do
            expect(DamageSearch.new(1..3).apply_to(items)).to eq [item_wood_spruce,
                                                                  item_wood_birch,
                                                                  item_wood_jungle]
            expect(DamageSearch.new([1, 3]).apply_to(items)).to eq [item_wood_spruce,
                                                                    item_wood_jungle]
          end

        end
        
      end

      describe '#==' do

        it 'should return true if the damage value matches' do
          expect(DamageSearch.new(1)).to eq DamageSearch.new(1)
          expect(DamageSearch.new(3..7)).to eq DamageSearch.new(3..7)
        end

        it 'should return false if the damage value does not match' do
          expect(DamageSearch.new(4)).not_to eq DamageSearch.new(3)
        end
        
      end
      
    end

    describe ItemIDSearch do

      describe '#initialize' do

        it 'should set the appropriate instance variables' do
          expect(ItemIDSearch.new(1).item_id).to eq 1
          expect(ItemIDSearch.new(2..4).item_id).to eq 2..4
        end

      end

      describe '#apply_to' do
        let(:item_log) { Item.new('Log', 17) }
        let(:item_stone) { Item.new('Stone', 1) }
        let(:item_gold_ore) { Item.new('Gold Ore', 14) }
        let(:item_iron_ore) { Item.new('Iron Ore', 15) }

        let(:item_wood_oak) { Item.new('Oak Wood', 17, 0) }
        let(:item_wood_spruce) { Item.new('Spruce Wood', 17, 1) }
        let(:item_wood_birch) { Item.new('Birch Wood', 17, 2) }
        let(:item_wood_jungle) { Item.new('Jungle Wood', 17, 3) }
        
        let(:items) { [item_stone,
                       item_gold_ore, item_iron_ore,
                       item_wood_oak, item_wood_spruce,
                       item_wood_birch, item_wood_jungle] }

        context 'with an integer' do

          it 'should return items with matching item ID' do
            expect(ItemIDSearch.new(17).apply_to(items)).to eq [item_wood_oak,
                                                                item_wood_spruce,
                                                                item_wood_birch,
                                                                item_wood_jungle]
  
            expect(ItemIDSearch.new(14).apply_to(items)).to eq [item_gold_ore]
          end

        end

        context 'with a range or array' do

          it 'should return items the item ID of which is included in the range or array' do
            expect(ItemIDSearch.new(14..15).apply_to(items)).to eq [item_gold_ore,
                                                                    item_iron_ore]

            expect(ItemIDSearch.new([1, 14]).apply_to(items)).to eq [item_stone,
                                                                     item_gold_ore]
          end

        end

        describe '#==' do

          it 'should return true if the item ID matches' do
            expect(ItemIDSearch.new(1)).to eq ItemIDSearch.new(1)
            expect(ItemIDSearch.new(3..7)).to eq ItemIDSearch.new(3..7)
          end

          it 'should return false if the item ID does not match' do
            expect(ItemIDSearch.new(4)).not_to eq ItemIDSearch.new(3)
          end
          
        end
        
      end

    end

    describe InputSearch do
      let(:item_log) { Item.new('Log', 17) }

      let(:item_iron_ore) { Item.new('Iron Ore', 15) }

      let(:item_wood_oak) { Item.new('Oak Wood', 17, 0) }

      let(:item_planks) { Item.new('Wood Planks', 5) }
      let(:item_coal) { Item.new('Coal', 263) }
      let(:item_stick) { Item.new('Stick', 280) }
      let(:item_torch) { Item.new('Torch', 50) }

      let(:recipe_planks) { Recipe.new('Wood Planks', { item_wood_oak => 1 }, { item_planks => 4 }) }
      let(:recipe_sticks) { Recipe.new('Sticks', { item_planks => 2 }, { item_stick => 4 }) }
      let(:recipe_torch) { Recipe.new('Torch', { item_stick => 1, item_coal => 1 }, { item_torch => 4 }) }

      let(:recipes) { [recipe_planks, recipe_sticks, recipe_torch] }
      
      describe '#initialize' do

        it 'should set the approrpiate instance variables' do
          expect(InputSearch.new(item_iron_ore).item).to eq item_iron_ore
        end

      end

      describe '#apply_to' do

        it 'should return recipes which have the item as input' do
          search = InputSearch.new(item_coal)
          expect(search.apply_to(recipes)).to eq [recipe_torch]
        end

      end

      describe '#==' do

        it 'should return true if the item is equal' do
          expect(InputSearch.new(item_iron_ore)).to eq InputSearch.new(Item.new('Iron Ore', 15))
        end

        it 'should return false if the item is not equal' do
          expect(InputSearch.new(item_torch)).not_to eq InputSearch.new(Item.new('Stone', 1))
        end
        
      end

    end

    describe OutputSearch do
      let(:item_log) { Item.new('Log', 17) }

      let(:item_iron_ore) { Item.new('Iron Ore', 15) }

      let(:item_wood_oak) { Item.new('Oak Wood', 17, 0) }

      let(:item_planks) { Item.new('Wood Planks', 5) }
      let(:item_coal) { Item.new('Coal', 263) }
      let(:item_stick) { Item.new('Stick', 280) }
      let(:item_torch) { Item.new('Torch', 50) }

      let(:recipe_planks) { Recipe.new('Wood Planks', { item_wood_oak => 1 }, { item_planks => 4 }) }
      let(:recipe_sticks) { Recipe.new('Sticks', { item_planks => 2 }, { item_stick => 4 }) }
      let(:recipe_torch) { Recipe.new('Torch', { item_stick => 1, item_coal => 1 }, { item_torch => 4 }) }
      let(:recipes) { [recipe_planks, recipe_sticks, recipe_torch] }
      
      describe '#initialize' do

        it 'should set the appropriate instance variables' do
          expect(OutputSearch.new(item_torch).item).to eq item_torch
        end

      end

      describe '#apply_to' do

        it 'should return recipes which have the item as output' do
          search = OutputSearch.new(item_torch)
          expect(search.apply_to(recipes)).to eq [recipe_torch]
        end

      end
pp
      describe '#==' do

        it 'should return true if the item is equal' do
          expect(OutputSearch.new(item_torch)).to eq OutputSearch.new(Item.new('Torch', 50))
        end
pp
        it 'should return false if the item is not equal' do
          expect(OutputSearch.new(item_torch)).not_to eq OutputSearch.new(Item.new('Stone', 1))
        end
        
      end
        
    end

  end

end


