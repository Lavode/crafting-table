#coding: utf-8

require_relative '../../spec_helper'

module CraftingTable

  module Search

    describe SearchBuilder do
      let(:search_for_name) do
        search = SearchBuilder.new
        search.name = 'Oak Wood'
        search.case_sensitive = true
        search.exact = true
        search
      end

      let(:search_for_name_fuzzy) do
        search = SearchBuilder.new
        search.name = 'Oak Wood'
        search.case_sensitive = false
        search.exact = false
        search
      end

      let(:search_for_item_id) do
        search = SearchBuilder.new
        search.item_id = 1
        search
      end

      let(:search_for_item_id_range) do
        search = SearchBuilder.new
        search.item_id = 5..10
        search
      end

      let(:search_for_damage_value) do
        search = SearchBuilder.new
        search.damage_value = [2, 4]
        search
      end

      let(:search_for_multiple_conditions) do
        search = SearchBuilder.new
        search.item_id = 1
        search.name = 'Oak Wood'
        search
      end

      let(:search_for_input) do
        search = SearchBuilder.new
        search.input = Item.new('Stone', 1)
        search
      end

      let(:search_for_output) do
        search = SearchBuilder.new
        search.output = Item.new('Torch', 50)
        search
      end

      describe '#searches' do

        it 'should return a NameSearch if name and exact were set' do
          expect(search_for_name.searches).to eq [NameSearch.new('Oak Wood',
                                                                 case_sensitive: true)]
        end
        
        it 'should return a FuzzyNameSearch if name was set, and exact was set to false' do
          expect(search_for_name_fuzzy.searches).to eq [FuzzyNameSearch.new('Oak Wood',
                                                                            case_sensitive: false)]
        end

        it 'should return a ItemIDSearch if item_id was set' do
          expect(search_for_item_id.searches).to eq [ItemIDSearch.new(1)]
          expect(search_for_item_id_range.searches).to eq [ItemIDSearch.new(5..10)]
        end

        it 'should return a DamageSearch if damage_value was set' do
          expect(search_for_damage_value.searches).to eq [DamageSearch.new([2, 4])]
        end

        it 'should return an InputSearch if input was set' do
          expect(search_for_input.searches).to eq [InputSearch.new(Item.new('Stone', 1))]
        end

        it 'should return an OutputSearch if output was set' do
          expect(search_for_output.searches).to eq [OutputSearch.new(Item.new('Torch', 50))]
        end

        it 'should return multiple searches if multiple conditions were set' do
          expect(search_for_multiple_conditions.searches).to eq [NameSearch.new('Oak Wood',
                                                                                case_sensitive: true),
                                                                 ItemIDSearch.new(1)]
        end
        
      end
      
        
    end

  end

end
