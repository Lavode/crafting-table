# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = 'crafting_table'
  spec.version       = '0.3.0'
  spec.author         = 'Michael Senn'
  spec.email         = ['morrolan@morrolan.ch']
  spec.description   = %q{Crafting assistant for Minecraft.}
  spec.summary       = %q{A library allowing to calculate the required amount of resources to craft item(s) in Minecraft.}
  spec.homepage      = 'https://bitbucket.org/Lavode/crafting-table'
  spec.license       = 'Apache 2.0'

  spec.files         = `git ls-files`.split($/)
  spec.test_files    = spec.files.grep(%r{^(spec|features)/})

  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'cucumber'
  spec.add_development_dependency 'yard'
end
